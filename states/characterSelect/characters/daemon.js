var Omnom = require('omnom');

module.exports = {
    thisPlayer: {
        events: {
        },
        properties: {
            m_timeRemaining: 300,
            m_name: 'daemon',
            m_timerInterval: null,

            init: function init(){
                var that = this;
                for(var name in this.m_events){
                    Omnom.omnomEventBus().listen(name, this[this.m_events[name]], this);
                }

                this.m_timerInterval = setInterval(function(){that.decrementTimer();}, 1000);
                
            },
            //lockTiles now simply decrements a portion of time remaining
            lockTiles: function lockTiles(eventData){
                if(eventData.source == this){
                    return;
                }
                this.m_timeRemaining -= 3*eventData.numLocked;
                this.dispatchCommand('updateTimer', 'self', {timeRemaining: this.timeRemaining});
                if(this.timeRemaining <= 0){
                    
                    this.dispatchCommand('gameOver', 'self',  {loser: true});
                    //clearInterval(this.m_timerInterval);
                }
            },
            //in addition to locking enemy tiles, a portion of the score CAN go to incrementing timeRemaining
            detectMatches : function detectMatches(map) {
                var isFirstDetection = false;
                if (typeof map == 'boolean'){
                    isFirstDetection = map;
                    map = null;
                }

                map = map || this.m_gridMap;
                var matchList = {
                    knownMatches: [], //list of coordinate sets
                    colorsPoints: {} //how many tiles of each color will be removed.
                };
                
                for (var coordX = 0; coordX < 9; coordX++) {
                    for (var coordY = 0; coordY < 15; coordY++) {
                        matchList = this.checkForMatchesAroundVertex(map, coordX + ',' + coordY, matchList);
                    }
                }

                if(matchList.knownMatches.length && map == this.m_gridMap && isFirstDetection){
                    this.m_timeRemaining += 2;
                    this.dispatchCommand('updateTimer', 'self', {timeRemaining: this.m_timeRemaining});
                    Omnom.omnomEventBus().dispatch('lockTiles', {source: this, numLocked: 1});
                }
                
                return matchList;
            },
            decrementTimer: function decrementTimer(){
                this.m_timeRemaining -= 1;
                this.dispatchCommand('updateTimer', 'self', {timeRemaining: this.m_timeRemaining});
                if(this.m_timeRemaining <= 0){

                    this.dispatchCommand('gameOver', 'self',  {loser: true});
                    //clearInterval(this.m_timerInterval);
                }
            }

        }
    },
    thatPlayer: {
        events: {},
        properties: {}
    }
};