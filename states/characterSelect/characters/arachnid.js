var Omnom = require('omnom');

module.exports = {
    thisPlayer: {
        events: {},
        properties: {
            m_name: "arachnid",
            rotations: 1,
            m_manyHandsTileThreshhold: 30,
            m_manyHandsTileWeights : {
                cyan: 0,
                pink: 0,
                green: 0,
                orange: 0,
                blue: 0
            },
            m_otherHandsActive: false,
            m_stickyWebTileThreshhold: 3,
            m_stickyWebTileWeights : {
                cyan: 0,
                pink: 0,
                green: 0,
                orange: 0,
                blue: 0
            },
            rotate : function rotate(data){
                var validPos = this.validatePosition(this.m_vertexPosition, data.position);

                if(!validPos){
                    //@todo set up an error message here.
                    return false;
                }
                var vertexList = [this.m_vertexPosition];
                if(this.m_otherHandsActive){
                    vertexList.push([
                        Math.floor(Math.random() * 8) + 1,
                        Math.floor(Math.random() * 9) + 1
                    ]);
                }

                data.tweenTime = this.m_tweenTime;

                data.position = [];
                for(var i = 0; i < vertexList.length; i++){
                    var rotationAttempt = this.resolveRotation(data.direction, this.m_gridMap, vertexList[i]);
                    if(rotationAttempt){
                        data.position.push(vertexList[i][0], vertexList[i][1]);
                    }
                } 

                if(data.position){
                    this.dispatchCommand('updateCursorRotated', 'self', data);
                }

                return rotationAttempt;
            },
            resolveMatches : function resolveMatches(knownMatches) {
                var i, j;
                var remainingTiles = -1;
                var that = this;
                
                //iterate through the map twice.  once to remove matched tiles, and again to settle the remaining ones into place.
                that.m_gridMap.forEach(function(curColumn, colIndex, map ) {
                    that.m_gridMap[colIndex] = curColumn.filter(function(testTileColor, tileIndex, col) {
                        var result = true;
                        var testCoords = colIndex + ',' + tileIndex;
                        if(~knownMatches.indexOf(testCoords)){
                            var manyhandsIndex = that.m_gridMap[colIndex][tileIndex].indexOf('_manyhands');
                            if(~manyhandsIndex){
                                that.triggerManyHands();
                            }

                            var stickywebIndex = that.m_gridMap[colIndex][tileIndex].indexOf('_stickyweb');
                            if(~stickywebIndex){
                                that.triggerStickyWeb();
                            }

                            var lockedIndex = that.m_lockedMap.indexOf(testCoords);
                            if(~lockedIndex){
                                that.m_lockedMap.splice(lockedIndex, 1);
                                that.m_lockedMap = that.m_lockedMap.map(function(lockedTile){
                                    var lockedCoords = lockedTile.split(',');
                                    if(lockedCoords[0] === colIndex && lockedCoords[1] > tileIndex){
                                        lockedCoords[1]--;
                                    }
                                    return lockedCoords[0] + ',' + lockedCoords[1];
                                });
                            }

                            result = false;
                        }

                        return result;
                    });
                    
                    
                    if (!~remainingTiles || that.m_gridMap[colIndex].length < remainingTiles) {
                        remainingTiles = that.m_gridMap[colIndex].length;
                    }
                });

                this.replenishTiles(knownMatches);

                this.dispatchCommand('updateRemoveTiles', 'self', {tileList: knownMatches, tweenTime: this.m_tweenTime});
                    
                return remainingTiles;
            },
            replenishTiles: function replenishTiles(matchList){
                var that = this;
                //first figure out which columns need more tiles and how many in each one.
                var colList = [];
                var colDict = {};

                var sortColList = function sortColList(a,b){ return a - b ; };

                for(var matchIndex = 0; matchIndex < matchList.length ; matchIndex++){
                    var coords = matchList[matchIndex].split(',');
                    var coordX = coords[0];
                    if(!~colList.indexOf(coordX)){
                        colList.push(coordX);

                        if(!colDict.hasOwnProperty(coordX)){
                            colDict[coordX] = 0;
                        }

                        colDict[coordX]++;
                    }
                }

                colList.sort(sortColList);

                /**
                 * now for each column we do a replenish.
                 * but we're gonna do some wacky shit to adjust the weights in 
                 * advance to reduce the likelyhood of cascades.
                 */
                var findColorWeightIndex = function findColorWeightIndex(color){
                    var result;
                    for(var i = 0; i < this.m_baseColorWeights.length; i++){
                        if(this.m_baseColorWeights[i].color == color){
                            result = i;
                            break;
                        }
                    }

                    return result;
                };

                var trimWeights = function trimWeights(){
                    var result = true;

                    var evenCol = (col%2 === 0);
                    var sideMatch = evenCol ? row : row-1;

                    if( (col-1) >= 0 && sideMatch >= 0){
                        var matchPrev = that.m_gridMap[col][row-1]; //the block under this one;
                        var matchPrev2 = that.m_gridMap[col-1][sideMatch]; //the first side tile that this could match with.
                        var matchPrev3 = that.m_gridMap[col-1][sideMatch + 1]; // second side tile this could match with

                        /**
                         * @todo add potential matchNext?
                         */
                        if(matchPrev == matchPrev2){
                            that.adjustWeight(matchPrev);
                        } else if ( matchPrev2 == matchPrev3){
                            that.adjustWeight(matchPrev2);
                        }
                    }
                };

                
                for(var colIndex = 0; colIndex < colList.length; colIndex++){
                    var col = +(colList[colIndex]);

                    var boardHeight = (col % 2 === 0) ? 8 : 9;
                    //starting point
                    var startingPoint = this.m_gridMap[colList[colIndex]].length;

                    for( var row = startingPoint; row < boardHeight; row++){
                        trimWeights();
                        var chosenColor = this.generateRandomTile();

                        if(this.m_manyHandsTileWeights[chosenColor] >= this.m_manyHandsTileThreshhold){
                            this.m_manyHandsTileWeights[chosenColor] = 0;
                            this.m_stickyWebTileWeights[chosenColor]++;
                            chosenColor += '_manyhands';
                        } else if (this.m_stickyWebTileWeights[chosenColor] >= this.m_stickyWebTileThreshhold){
                            this.m_stickyWebTileWeights[chosenColor] = 0;
                            this.m_manyHandsTileWeights[chosenColor]++
                            chosenColor += '_stickyweb';    
                        } else {
                            var whichGetsIncremented = Math.floor(Math.random() * 3 + 1);
                            switch(whichGetsIncremented){
                                case 1: 
                                    this.m_manyHandsTileWeights[chosenColor]++;
                                    break;
                                case 2: 
                                    this.m_stickyWebTileWeights[chosenColor]++;
                                    break;
                                case 3:
                                    this.m_manyHandsTileWeights[chosenColor]++;
                                    this.m_stickyWebTileWeights[chosenColor]++;
                                    break;
                            }
                                    
                        }

                        this.addTile(chosenColor, col);
                    }
                }
            },
            triggerManyHands: function triggerManyHands(){
                var that = this;
                this.m_otherHandsActive = true

                setTimeout(function(){
                    that.m_otherHandsActive = false;
                }, 10000)
            },
            triggerStickyWeb: function triggerStickyWeb(){
                Omnom.omnomEventBus().dispatch('stickyWebTriggered', {});
            }

        },
    },
    thatPlayer: {
        events: {
            'stickyWebTriggered': 'handleStickyWeb'
        },
        properties: {
            handleStickyWeb: function handleStickyWeb(){
                this.dispatchCommand('webShot', 'self', {coords: this.m_vertexPosition});
                var rotateHolder = this.rotate;
                var moveHolder = this.moveCursor;

                var that = this;

                this.rotate = this.crapstuck;
                this.moveCursor = this.crapstuck;

                setTimeout(function(){
                    that.rotate = rotateHolder;
                    that.moveCursor = moveHolder;
                    that.dispatchCommand('webShotEnd', 'self', {});
                }, 10000);
            },
            crapstuck: function crapstuck(){
                this.dispatchCommand('webStuck', 'self', {});
            }
        },
    }
};