//get the port to start listening on
GAME_PORT = process.argv[2] || 82;

var characterSelectState = function characterSelectState(){
    this.name = 'characterSelect';
    this.dependencies = ['Player'];

    var players = [];
    var readyPlayers = 0;

    this.init = function init(initPromise){
        //EventBus handlers
        this.sandbox.listen('omnomNet_newConnection', this.handleNewConnection, this);
        //this.sandbox.listen('returnToMainMenu', this.handleNewConnection, this);

        //OmnomNet connection listener
        this.sandbox.startListening(GAME_PORT);
        initPromise.resolve();
    };


    this.handleNewConnection = function handleNewConnection(conn){
        var that = this;

        var newPlayer = new (this.sandbox.Player)(conn);
        
        players.push(newPlayer);


        this.setupEvents(newPlayer);

        if(players.length === 2){
            this.signalReady();
        }
    };

    this.setupEvents = function setupEvents(player){
        var that = this;

        player.connection.reset();

        player.connection.on('characterSelected', function(data){
            that.selectCharacter(data, player);
        }, this);

        player.connection.on('powerSelected', function(data){
            that.selectPower(data, player);
        }, this);

        /**
         * @todo this might make me need to move 'players' into something within scope of the object?
         */
        player.connection.on('countdownComplete', function(){
            
        });
    };

    this.signalReady = function signalReady(){
        //everything is well and good, tell the client to switch states to the main menu.
        for(var playerIndex = 0; playerIndex < players.length; playerIndex++){
            var commandObj =  JSON.stringify({
                command: 'stateChange',
                data: {
                    statename: "CharacterSelect"
                }
            });
            players[playerIndex].connection.command('write', commandObj);
        }
            
    };

    this.beginCountdown = function beginCountdown(){
        var that = this;
        //everything is well and good, tell the client to switch states to the gameboard.
        var intervals = [];

        var spliceInterval = function spliceInterval(index){
            index = intervals.length > index ? index : 0;

            clearInterval( intervals[index]);

            intervals.splice(index, 1);

            if(intervals.length === 0){
                that.sandbox.dispatch('readySetFight', players);    
            }
        }

        for(var playerIndex = 0; playerIndex < players.length; playerIndex++){
            var commandObj =  JSON.stringify({
                command: 'beginCountdown',
                data: {}
            });
            players[playerIndex].connection.command('write', '##' + commandObj);

            var intervalFunc = function(){
                var timeRemaining = 5;
                var index = playerIndex;

                return function(){
                    var countdownCommandObj = JSON.stringify({
                        command: 'countdown',
                        data: {timeRemaining: timeRemaining--}
                    });

                    

                    if(timeRemaining >= 0){
                        players[index].connection.command('write', '##' + countdownCommandObj);
                    } else {
                        spliceInterval(index);
                    }
                }
            }
                
            intervals.push(setInterval(intervalFunc(), 1000));
        }
            
    };

    /**
     * @todo  select the character!  set it to the player object.
     */
    this.selectCharacter = function selectCharacter(data, player){
        player.selectedCharacter = data.selectedCharacter;

        var curPlayer = players.indexOf(player);
        var otherPlayer = curPlayer === 0 ? 1 : 0;

        consumePowers(curPlayer, otherPlayer, data.selectedCharacter);
        
        //tell the current player that his selection was acknowledged
        players[curPlayer].connection.command('write', '##' + JSON.stringify({
            command: 'characterAcknowledged',
            data: {
                character: data.selectedCharacter,
                name: player.m_name
            }
        }));

        //tell the other player that a character was selected.
        players[otherPlayer].connection.command('write', '##' + JSON.stringify({
            command: 'characterSelected',
            data: {
                character: data.selectedCharacter,
                name: player.m_name
            }
        }));

        readyPlayers++;

        if(readyPlayers === 2){
            this.beginCountdown();
        }
    };

    /**
     * @todo  select the character!  set it to the player object.
     */
    this.selectPower = function selectPower(data, player){
        player.selectedPower = data.selectedPower;
        
        var curPlayer = players.indexOf(player);
        var otherPlayer = curPlayer === 0 ? 1 : 0;

        //tell the current player that his selection was acknowledged
        players[curPlayer].connection.command('write', '##' + JSON.stringify({
            command: 'powerAcknowledged',
            data: {
                power: data.selectedPower
            }
        }));

        //notify the OTHER player what was selected.
        players[otherPlayer].connection.command('write', '##' + JSON.stringify({
            command: 'powerSelected',
            data: {
                power: data.selectedPower
            }
        }));

        

        if(readyPlayers === 2){
            this.beginCountdown();
        }
    };

    var consumePowers = function consumePowers(thisPlayer, thatPlayer, charNum){
        var powerSets = {
            1: 'spellbinder',
            2: 'warden',
            3: 'kaptinkaos',
            4: 'glasscannon',
            5: 'enlightened',
            6: 'daemon',
            7: 'scrappy',
            8: 'arachnid'
        };

        var whichChar = powerSets[charNum];
console.log("CHARACTER " + whichChar + "PICKED");
        var powerData;

        if(whichChar == 'scrappy'){
            var actualPowerSets = {
                1: 'spellbinder',
                2: 'warden',
                3: 'kaptinkaos',
                4: 'glasscannon',
                5: 'enlightened',
                6: 'daemon',
                7: 'arachnid'
            };

            var realCharacter = Math.floor(Math.random() * 7) + 1;
            randomChar = actualPowerSets[realCharacter];
console.log("PICKING SCRAPPY GOT ME " + randomChar + " POWERS");
            powerData = require('./characters/' + randomChar + '.js');

            powerData.thisPlayer.m_name = 'scrappy';


        } else {
            powerData = require('./characters/' + whichChar + '.js');
        }



        var prop;
        var eventName;
        //first deal with adding stuff to the plauyer who picked the character.
        for(prop in powerData.thisPlayer.properties){
            players[thisPlayer][prop] = powerData.thisPlayer.properties[prop];
        }

        for(eventName in powerData.thisPlayer.events){
            players[thisPlayer]['m_events'][eventName] = powerData.thisPlayer.events[eventName];
        }

        //now for the opponent.
        for(prop in powerData.thatPlayer.properties){
            players[thatPlayer][prop] = powerData.thatPlayer.properties[prop];
        }

        for(eventName in powerData.thatPlayer.events){
            players[thatPlayer]['m_events'][eventName] = powerData.thatPlayer.events[eventName];
        }
    };


};

module.exports = characterSelectState;