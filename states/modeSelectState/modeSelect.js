var modeSelectState = function modeSelectState(){
    this.players = [];

    this.dependencies = ['omnomNet', 'omnomFlash'];
    this.name = "mainMenu";


    this.init = function init(initPromise){

        this.sandbox.listen('enterModeSelect', this.handleNewPlayer, this);
        //this.sandbox.listen('r')

        /*this.sandbox.listen('stateChange.mainMenu', this.handleStateChange, this);
        this.sandbox.listen('returnToMainMenu', this.handleNewConnection, this);*/

        initPromise.resolve();
    };

    this.handleNewPlayer = function handleNewPlayer(player){
        var that = this;
        player.connection.reset();
        this.players.push(player);

        //everything is well and good, tell the client to switch states to the main menu.
        var commandObj =  JSON.stringify({
            command: 'stateChange',
            data: {
                statename: "ModeSelect"
            }
        });        
        player.connection.command('write', commandObj);

        player.connection.on('searchForOpponent', function(data){
            switch(data.searchType){
                case 'random':
                    that.searchForOpponent(player, "random");
                    break;    
                default:
                    //harsh
                    throw new Error('INVALID SEARCH TYPE');
                    break;
            }
            
        }, this);

    };

    this.searchForOpponent = function searchForOpponent(player, searchType){
        this.players.splice(this.players.indexOf(player), 1);

        /**
         * @todo change the event thrown based on search type, or modify the event data to include the searchType?
         */
        this.sandbox.dispatch('beginMatchMaking', player);
    }

};

module.exports = modeSelectState;