

var simulationState = function simulationState(){
    this.name = 'simulation';
    this.dependencies = ['Player'];

    gameOver = false;

    var players = [];

    /**
     * initialize the state;
     */
    this.init = function init(initPromise){
        this.sandbox.listen('readySetFight', this.handleNewConnection, this);
        this.sandbox.listen('needMoreTiles', this.requestTiles, this);
        this.sandbox.listen('playerCommand', this.handleNewPlayerCommand, this);
        initPromise.resolve();
    };

    /**
     * sandbox eventHandlers
     * @param  Vector.<Player> newPlayers 
     */
    this.handleNewConnection = function handleNewConnection(newPlayers){
        players = newPlayers;

        for(var playerIndex = 0; playerIndex < players.length; playerIndex++){
            this.setupPlayerEvents(players[playerIndex]);
        }

        this.enterState();
    };

    this.setupPlayerEvents = function setupPlayerEvents(player){
        var that = this;

        player.connection.reset();

        player.connection.on('updateCursorMoved', function(data){
            var valid = player.moveCursor(data);
            //that.moveCursor(valid, players.indexOf(player), data);
        }, this);

        player.connection.on('updateCursorRotated', function(data){
            that.rotate(data, players.indexOf(player));
        }, this);

        player.init();
    };


    this.enterState = function enterState(){
        var that = this;
        for(var playerIndex = 0; playerIndex < players.length; playerIndex++){
            var commandObj = JSON.stringify({
                command: 'stateChange',
                data: {statename: "Game"}
            });
            players[playerIndex].connection.command('write', '##' + commandObj);
            //players[playerIndex].initializeTiles(true);
        }
        var intervals = [];
        for(playerIndex = 0; playerIndex < players.length; playerIndex++){
            players[playerIndex].initializeTiles(playerIndex);

            var intervalFunc = function(){
                var timeRemaining = 6;
                var index = playerIndex;
                return function(){
                    var countdownCommandObj = JSON.stringify({
                        command: 'countdown',
                        data: {timeRemaining: --timeRemaining}
                    })
                    if(timeRemaining >= 0){
                        players[index].connection.command('write', '##' + countdownCommandObj);    
                    } else {
                        clearInterval(intervals[index]);
                    }  
                }
            }
                

            intervals.push(setInterval(intervalFunc(), 1000));
        }
        //this.requestTiles(true);
    };

    this.rotate = function rotate(data, whichPlayer){
        var thisPlayer = whichPlayer;
        var thatPlayer = thisPlayer ? 0 : 1;

        for(var numRotations = 0; numRotations <=2; numRotations++){
            var attemptRotation = players[thisPlayer].rotate(data);

            //if some of the tiles were locked, just kill the rotation attempt here.
            if(!attemptRotation){
                break;
            }

            var continueRotation = true;

            //on the final rotation, the tiles resume their original position.  no reason to check for matches again.
            if(numRotations < 2){
                matchList = players[thisPlayer].detectMatches(true);

                while(matchList.knownMatches.length > 0){
                    continueRotation = false;
                    
                    players[thisPlayer].resolveMatches(matchList.knownMatches);
                    //check for cascade to determine if the whileloop should continue.
                    matchList = players[thisPlayer].detectMatches();
                }

            }

            //if we detected matches, no reason to keep rotating.
            if(!continueRotation){
                break;
            }
        }
    };

    this.handleNewPlayerCommand = function handleNewPlayerCommand(commandData){
        if(gameOver){
            return;
        }

        var that = this;
        var thisPlayer = players.indexOf(commandData.source);
        var thatPlayer = thisPlayer === 0 ? 1 : 0;

        //clientside player targets. each player client is playuer 1 in their own perspective.
        var thisClientPlayer;
        var thatClientPlayer;

        switch(commandData.target){
            case 'self':
                thisClientPlayer = 0;
                thatClientPlayer = 1;
                break;
            case 'opponent':
                thisClientPlayer = 1;
                thatClientPlayer = 0;
                break;
        }

        delete commandData.target;
        delete commandData.source;

        switch (commandData.command){
            case 'gameOver':
                gameOver = true;

                var endProc = function endProc(){
                    this.m_numClosed = that.m_numClosed || 0;
                    this.m_numClosed++;

                    if(this.m_numClosed ==2 ){
                        process.exit();
                    }
                };

                players[thisPlayer].connection.addListener('end', function(){
                    endProc.call(that);
                });
                players[thatPlayer].connection.addListener('end', function(){
                    endProc.call(that);
                });

                players[thisPlayer].connection.command('write', '##' + JSON.stringify(commandData));
                players[thisPlayer].connection.command('end');

                delete commandData.data.loser;
                players[thatPlayer].connection.command('write', '##' + JSON.stringify(commandData));
                players[thatPlayer].connection.command('end');
                break;
            default:

                commandData.data.player = thisClientPlayer;
                players[thisPlayer].connection.command('write', '##' + JSON.stringify(commandData));

                commandData.data.player = thatClientPlayer;
                players[thatPlayer].connection.command('write', '##' + JSON.stringify(commandData));
                break;
        }

            

    };
        
};

module.exports = simulationState;
