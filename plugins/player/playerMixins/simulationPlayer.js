var Omnom = require('omnom');

//set game info
var BOARD_HEIGHT = 10;
var BOARD_WIDTH = 10;
var BLOCK_SIZE = 45;
var LOW_COL_HEIGHT_THRESHOLD = 30;

var properties = {
    m_tweenTime: 0.35,
    m_vertexPosition: [0,0],
    m_gridMap : [],
    m_lockedMap : [],
    m_events: {
        'lockTiles' : 'lockTiles'
    },
    m_gameOver: false,
    //this function gets overridden when power tiles are added.
    m_baseColorWeights : [
        {color: 'cyan', weight: 20},
        {color: 'pink', weight: 20},
        {color: 'green', weight: 20},
        {color: 'orange', weight: 20},
        {color: 'blue', weight: 20}
    ],
    init: function init(){
        for(var name in this.m_events){
            Omnom.omnomEventBus().listen(name, this[this.m_events[name]], this);
        }
        
    },
    adjustWeight : function adjustWeight(chosenColor){
        var sortWeights = function sortWeights(a,b){
            return a.weight - b.weight;
        };

        for(var weightIndex = 0; weightIndex < this.m_baseColorWeights; weightIndex++){
            cw = this.m_baseColorWeights[weightIndex];
            if(cw.color == chosenColor){
                cw.weight -= (this.m_baseColorWeights.length - 1);
            } else {
                cw.weight++;
            }
        }

        this.m_baseColorWeights.sort(sortWeights);
    },
    initializeTiles : function initializeTiles(playerNum){
        var allowedColorWeights;
        var that = this;

        var bcwFilter = function bcwFilter(cw){
            var result = true;

            var evenCol = (col%2 === 0);
            var sideMatch = evenCol ? row : row-1;



            if( (col-1) >= 0 && sideMatch >= 0){

                var matchPrev = that.m_gridMap[col] ?  that.m_gridMap[col][row-1] : ''; //the block under this one;
                var matchPrev2 = that.m_gridMap[col-1][sideMatch]; //the first side tile that this could match with.
                var matchPrev3 = that.m_gridMap[col-1][sideMatch + 1]; // second side tile this could match with

                if(matchPrev == matchPrev2 && matchPrev == cw.color) {
                    result = false;
                } else if (matchPrev2 == matchPrev3 && matchPrev2 == cw.color){
                   result = false;
                }
            }

            return result;
        };


        for(var col = 0; col < BOARD_WIDTH; col++){

            var boardHeight = (col % 2 === 0) ? 8 : 9;
            
            for(var row = 0; row < boardHeight; row++){
                allowedColorWeights = this.m_baseColorWeights.filter(bcwFilter);

                var totalWeight = 0;
                for(var acwIndex = 0; acwIndex < allowedColorWeights.length; acwIndex++){
                    totalWeight += allowedColorWeights[acwIndex].weight;
                }

                var randomColor = Math.floor(Math.random() * totalWeight);

                var curWeight = 0;
                var chosenColor;

                for(var testColorIndex = 0; testColorIndex < allowedColorWeights.length; testColorIndex++){
                    curWeight += allowedColorWeights[testColorIndex].weight;

                    if(curWeight > randomColor){
                        chosenColor = allowedColorWeights[testColorIndex].color;
                        break;
                    }
                }
                this.adjustWeight(chosenColor);
                
                this.addTile(chosenColor, col);
            }
        }
    },
    generateRandomTile: function generateRandomTile(){
        var totalWeight = 0;
        for(var acwIndex = 0; acwIndex < this.m_baseColorWeights.length; acwIndex++){
            totalWeight += this.m_baseColorWeights[acwIndex].weight;
        }

        var randomColor = Math.floor(Math.random() * totalWeight);

        var curWeight = 0;
        var chosenColor;

        for(var testColorIndex = 0; testColorIndex < this.m_baseColorWeights.length; testColorIndex++){
            curWeight += this.m_baseColorWeights[testColorIndex].weight;

            if(curWeight > randomColor){
                chosenColor = this.m_baseColorWeights[testColorIndex].color;
                break;
            }
        }

        this.adjustWeight(chosenColor);

        return chosenColor;
    },
    replenishTiles: function replenishTiles(matchList){
        var that = this;
        //first figure out which columns need more tiles and how many in each one.
        var colList = [];
        var colDict = {};

        var sortColList = function sortColList(a,b){ return a - b ; };

        for(var matchIndex = 0; matchIndex < matchList.length ; matchIndex++){
            var coords = matchList[matchIndex].split(',');
            var coordX = coords[0];
            if(!~colList.indexOf(coordX)){
                colList.push(coordX);

                if(!colDict.hasOwnProperty(coordX)){
                    colDict[coordX] = 0;
                }

                colDict[coordX]++;
            }
        }

        colList.sort(sortColList);

        /**
         * now for each column we do a replenish.
         * but we're gonna do some wacky shit to adjust the weights in 
         * advance to reduce the likelyhood of cascades.
         */
        var findColorWeightIndex = function findColorWeightIndex(color){
            var result;
            for(var i = 0; i < this.m_baseColorWeights.length; i++){
                if(this.m_baseColorWeights[i].color == color){
                    result = i;
                    break;
                }
            }

            return result;
        };

        var trimWeights = function trimWeights(){
            var result = true;

            var evenCol = (col%2 === 0);
            var sideMatch = evenCol ? row : row-1;

            if( (col-1) >= 0 && sideMatch >= 0){
                var matchPrev =  that.m_gridMap[col][row-1]; //the block under this one;
                var matchPrev2 = that.m_gridMap[col-1][sideMatch]; //the first side tile that this could match with.
                var matchPrev3 = that.m_gridMap[col-1][sideMatch + 1]; // second side tile this could match with

                /**
                 * @todo add potential matchNext?
                 */
                if(matchPrev == matchPrev2){
                    that.adjustWeight(matchPrev);
                } else if ( matchPrev2 == matchPrev3){
                    that.adjustWeight(matchPrev2);
                }
            }
        };

        
        for(var colIndex = 0; colIndex < colList.length; colIndex++){
            var col = +(colList[colIndex]);

            var boardHeight = (col % 2 === 0) ? 8 : 9;
            //starting point
            var startingPoint = this.m_gridMap[colList[colIndex]].length;

            for( var row = startingPoint; row < boardHeight; row++){
                trimWeights();
                var chosenColor = this.generateRandomTile();
                this.addTile(chosenColor, col);
            }

                
            
        }
    },


    addTile : function addTile(color, col){
        this.m_gridMap[col] = this.m_gridMap[col] || [];
        this.m_gridMap[col].push(color);
        this.dispatchCommand('serverTilesSet', 'self', {color: color, column: col});
    },

    /**
     * handle moving the cursor here and checking against the client provided position.
     * @todo server should handle moving the cursor.
     */
    moveCursor : function moveCursor(data){
        var validPos = this.validatePosition(this.m_vertexPosition, data.position);
        if(!validPos){
            return false;
        }
        switch(data.direction.toUpperCase()) {
            case "RIGHT":
                if (this.m_vertexPosition[0] < 8) {
                    this.m_vertexPosition[0]++;
                }
                break;
            case "LEFT":
                if (this.m_vertexPosition[0] > 0) {
                    this.m_vertexPosition[0]--;
                }
                break;
            case "UP":
                if (this.m_vertexPosition[1] < 14) {
                    this.m_vertexPosition[1]++;
                }
                break;
            case "DOWN":
                if (this.m_vertexPosition[1] > 0) {
                    this.m_vertexPosition[1]--;
                }
                break;
            default:
                break;
        }
        this.dispatchCommand('updateCursorMoved', 'self', {position: this.m_vertexPosition});

        return true;
    },
    rotate : function rotate(data){
        var validPos = this.validatePosition(this.m_vertexPosition, data.position);

        if(!validPos){
            //@todo set up an error message here.
            return false;
        }


        data.tweenTime = this.m_tweenTime;
        

        var rotationAttempt = this.resolveRotation(data.direction, this.m_gridMap, this.m_vertexPosition);

        if(rotationAttempt){
            this.dispatchCommand('updateCursorRotated', 'self', data);
        }

        return rotationAttempt;
    },
    /**
     * validate the position of a cursor by comparing it to the local server state.
     * Assumes all transactions are sent in order, and should be treated as atomic.
     * @param  array serverPos coords representing the cursor's position on server
     * @param  array clientPos coords representing the cursor's position on client.
     * @return boolean
     */
    validatePosition : function validatePosition(serverPos, clientPos){
        var result = (clientPos[0] == serverPos[0] && clientPos[1] == serverPos[1]);

        return result;
    },

    /**
     * logically rotate tile values around a point.
     * @param  String direction Rotate Left or Rotate Right ('client values yay')
     * @param  array map       representation of a map.  this could be a reference to a real map, or a shallow copy.
     * @param  array coords    location of cursor
     */
    resolveRotation : function resolveRotation(direction, map, coords){
        var movingTiles = this.getTilesAroundVertex(coords);

        if (~this.m_lockedMap.indexOf(movingTiles[0][0] + "," + movingTiles[0][1]) ||
            ~this.m_lockedMap.indexOf(movingTiles[1][0] + "," + movingTiles[1][1]) ||
            ~this.m_lockedMap.indexOf(movingTiles[2][0] + "," + movingTiles[2][1])) {
                return false;
        }


        var hexRotation = direction;

        //the rotation sequence is flipped later on if the 2 big hex col is left.
        if (coords[0] % 2 != coords[1] % 2) {
            hexRotation = (hexRotation == INPUT_ROTATE_LEFT) ? INPUT_ROTATE_RIGHT : INPUT_ROTATE_LEFT;
        }

        /**
         * abstraction... awesome till it makes your eyes bleed.  --DM
         */
        var temp;
        switch(hexRotation) {
            case INPUT_ROTATE_LEFT:
                temp = map[movingTiles[0][0]][movingTiles[0][1]];
                map[movingTiles[0][0]][movingTiles[0][1]] = map[movingTiles[1][0]][movingTiles[1][1]];
                map[movingTiles[1][0]][movingTiles[1][1]] = map[movingTiles[2][0]][movingTiles[2][1]];
                map[movingTiles[2][0]][movingTiles[2][1]] = temp;
                break;
            case INPUT_ROTATE_RIGHT:
                temp = map[movingTiles[0][0]][movingTiles[0][1]];
                map[movingTiles[0][0]][movingTiles[0][1]] = map[movingTiles[2][0]][movingTiles[2][1]];
                map[movingTiles[2][0]][movingTiles[2][1]] = map[movingTiles[1][0]][movingTiles[1][1]];
                map[movingTiles[1][0]][movingTiles[1][1]] = temp;
                break;
        }
        return true;
    },
    /**
     * get list of coordinates indicating the tiles around the cursor, represented by cursor coordinates passed in.
     * @param  array coords vertex position of cursor
     * @return array        set of tile positions surrounding cursor.
     */
    getTilesAroundVertex : function getTilesAroundVertex(coords) {
        
        var biggerCol;
        var smallerCol;
        
        if (coords[0] % 2 == coords[1] % 2) {
            //column with two tiles is on the right
            biggerCol = +coords[0] + 1;
            smallerCol = +coords[0];
        } else {
            //column with two tiles is on the left
            biggerCol = +coords[0];
            smallerCol = +coords[0] + 1;
        }
        
        var middleTile = coords[1] % 2 ? Math.ceil(coords[1] / 2) : Math.floor(coords[1] / 2);
        
        var lowerTileIndex = Math.floor(coords[1] / 2);
        
        var resultTiles = [
            [biggerCol, lowerTileIndex],
            [smallerCol, middleTile],
            [biggerCol, +lowerTileIndex + 1]
        ];
        
        return resultTiles;
    },
    /**
     * detect matches on blocks surrounding a vertex position.  
     */
    detectMatches : function detectMatches(map) {
        var isFirstDetection = false;
        if (typeof map == 'boolean'){
            isFirstDetection = map;
            map = null;
        }

        map = map || this.m_gridMap;
        var matchList = {
            knownMatches: [], //list of coordinate sets
            colorsPoints: {} //how many tiles of each color will be removed.
        };
        
        for (var coordX = 0; coordX < 9; coordX++) {
            for (var coordY = 0; coordY < 15; coordY++) {
                matchList = this.checkForMatchesAroundVertex(map, coordX + ',' + coordY, matchList);
            }
        }

        if(matchList.knownMatches.length && map == this.m_gridMap && isFirstDetection){
            Omnom.omnomEventBus().dispatch('lockTiles', {source: this, numLocked: 1});
        }
        
        return matchList;
    },
    /**
     * check for matches around a given coord string.  since this function is called recursively, we need to ensure that
     * it short circuits if the coordstring is in the known matches.
     * @param matchList var matchList:Object = {
            knownMatches: [], //list of coordinate sets
            colorTiles: {} //how many vertexes of each color were detected.
        };
     */
    checkForMatchesAroundVertex : function checkForMatchesAroundVertex(map, coordString, matchList) {
        var coords = coordString.split(',');
        
        var coordX = coords[0];
        var coordY = coords[1];
        
        var matchTiles = this.getTilesAroundVertex(coords);

        var testColor = '';
        var newMatchSet = [];

        for(var matchTileIndex = 0; matchTileIndex < matchTiles.length ; matchTileIndex++){
            var testTilePos = matchTiles[matchTileIndex];
            newMatchSet.push(testTilePos[0] + ',' + testTilePos[1]);

            var tmpColor = map[testTilePos[0]][testTilePos[1]];
            tmpColor = tmpColor.split('_');
            tmpColor = tmpColor[0];

            //if it hasn't been set yet, set it as the basis to match by.
            if(!testColor){
                testColor = tmpColor;
            } else if (testColor !== tmpColor){
                testColor = '';
                break;
            }
        }

        var numAdded = 0;

        if(testColor){
            newMatchSet.forEach(function(newMatch){
                if(!~matchList['knownMatches'].indexOf(newMatch)){
                    matchList.colorsPoints[testColor] = matchList.colorsPoints[testColor] || 0;
                    matchList.colorsPoints[testColor]++;
                    matchList.knownMatches.push(newMatch);
                    numAdded++;
                }
            });
        }

        /*if(numAdded){
            var adjacentVertices = getAdjacentVertices(coordString);
            
            for (var j = 0; j < adjacentVertices.length; j++) {
                matchList = checkForMatchesAroundVertex(map, adjacentVertices[j], matchList);
            }
        }*/

        return matchList;
    },
    /**
     * get vertices adjacent to the vertex coordinates passed in
     * @param  String coordString ex: 3,4
     * @return array list of coord strings.
     */
    getAdjacentVertices : function getAdjacentVertices(coordString) {
        var coords = coordString.split(',');
        
        var coordX = coords[0];
        var coordY = coords[1];
        
        var result = [];
        if (coordY < 14) {
            result.push(coordX + ',' + (coordY + 1));
        }
        
        if (coordY > 0) {
            result.push(coordX + ',' + (coordY - 1));
        }
        
        
        var newX = -1;
        if ( ((coordX % 2) && (coordY % 2)) ||
            ((coordX % 2 !== 0) && (coordY % 2 !== 0)) &&
            coordX < 8) {
            newX = coordX + 1;
        }
        if (coordX > 0) {
            newX = coordX - 1;
        }
        
        if (newX > -1) {
            result.push(newX + ',' + coordY);
        }
        
        return result;
    },
    /**
     * rewritten copy of resolveMatches, hopefully like before, bruteforcing will solve a lot of the issues.
     * @param  [String] matchList
     */
    resolveMatches : function resolveMatches(knownMatches) {
        var i, j;
        var remainingTiles = -1;
        var that = this;
        
        //iterate through the map twice.  once to remove matched tiles, and again to settle the remaining ones into place.
        that.m_gridMap.forEach(function(curColumn, colIndex, map ) {
            that.m_gridMap[colIndex] = curColumn.filter(function(testTileColor, tileIndex, col) {
                var result = true;
                var testCoords = colIndex + ',' + tileIndex;
                if(~knownMatches.indexOf(testCoords)){
                    result = false;
                    var lockedIndex = that.m_lockedMap.indexOf(testCoords);
                    if(~lockedIndex){
                        that.m_lockedMap.splice(lockedIndex, 1);
                        //detect if a locked tile moved due to a match being made below it, and adjust accordingly.
                        that.m_lockedMap = that.m_lockedMap.map(function(lockedTile){
                            var lockedCoords = lockedTile.split(',');
                            if(lockedCoords[0] === colIndex && lockedCoords[1] > tileIndex){
                                lockedCoords[1]--;
                            }
                            return lockedCoords[0] + ',' + lockedCoords[1];
                        });
                    }
                    
                }

                return result;
            });
            
            
            if (!~remainingTiles || that.m_gridMap[colIndex].length < remainingTiles) {
                remainingTiles = that.m_gridMap[colIndex].length;
            }
        });

        this.replenishTiles(knownMatches);

        this.dispatchCommand('updateRemoveTiles', 'self', {tileList: knownMatches, tweenTime: this.m_tweenTime});
            
        return remainingTiles;
    },


    lockTiles : function lockTiles(eventData){
        if(eventData.source == this){
            return;
        }
        var numLocked = eventData.numLocked;
        //var numLocked = 30;

        var lockedTiles = [];

        //locking from the bottom up, so gotta iterate slightly differently
        for (var coordY = 0; coordY < 9 ; coordY++) {
            for (var coordX = 0; coordX < 10; coordX++) {
                var coordString = coordX + ',' + coordY;
                if(!~this.m_lockedMap.indexOf(coordString)){
                    lockedTiles.push(coordString);
                    this.m_lockedMap.push(coordString);
                    numLocked--;
                }

                if(numLocked === 0){
                    break;
                }
            }

            if(numLocked === 0){
                break;
            }
        }

        this.dispatchCommand('updateLockTiles', 'self', {tileList: lockedTiles});
        this.canStillMove();

        return;
    },

    canStillMove : function canStillMove() {
        
        var canMove = false;
        var shallowCopy = [];
        
        for (var col = 0; col < this.m_gridMap.length ; col++ ) {
            shallowCopy[col] = this.m_gridMap[col].slice();
        }

        for (var coordX = 0; coordX < 8 ; coordX++) {
            for (var coordY = 0; coordY < 14; coordY++) {
                canMove = this.simulateRotation(shallowCopy, [coordX, coordY]);
                if (canMove) {
                    break;
                }
            }
            
            if (canMove) {
                break;
            }
        }

        if(!canMove){
            this.dispatchCommand('gameOver', 'self', {loser: true});
            this.m_gameOver = true;
        }


        //return canMove;
    },

    /**
     * 
     * @return if a match was found.
     */
    simulateRotation : function simulateRotation(shallowCopy, coords) {
        
        var result = false;
        var matchList;
        
        //i is the number of rotations
        for (var i = 0; i < 3; i++) {
            var rotationAttempt= this.resolveRotation('RIGHT', shallowCopy, coords);
            
            //can't move because a piece is locked.
            if (!rotationAttempt) {
                break;
            }
            if (i < 2) {
                matchList = this.detectMatches(shallowCopy);
                if (matchList.knownMatches.length) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    },

    dispatchCommand: function dispatchCommand(command, target, data){
        if(this.m_gameOver){
            return;
        }
        var eventData = {
            command: command,
            target: target,
            data: data,
            source: this
        };


        Omnom.omnomEventBus().dispatch('playerCommand', eventData);

        /**
         * @todo  implement a promise resolvable from any method within the player.
         */
        /**
         * @todo  gotta figure out a way to set omnom promises here?
         */
    }
};

module.exports = function(player){
    for(var propName in properties){
        if(properties[propName] instanceof Function){
            player[propName] = properties[propName];
        } else if (properties[propName] instanceof Array){
            player[propName] = properties[propName].slice();
        } else if (properties[propName] instanceof Object){
            player[propName] = Object.create(properties[propName]);
        } else if (typeof properties[propName] == 'number'){
            player[propName] = 0 + properties[propName]
        } else if (typeof properties[propName] == 'boolean'){
            player[propName] = !!properties[propName];
        } else {
            console.log("NO TYPE FOUND ON MERGE:", typeof properties[propName]);
        }
    }
};