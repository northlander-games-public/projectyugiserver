

var flashPolicyServer = require('policyfile');


module.exports = {
    name: "omnomFlash",
    dependencies: ['omnomNet'],
    events: [],
    omnom: {
        fpsStartListening: function fpsStartListening(port){
            fps = flashPolicyServer.createServer();
            fps.listen(this.tcpServer);
        },
    },
    sandbox: {
        fpsStartListening: function sandboxFpsStartListening(){
            /**
             * @todo add validation to the port.
             */
            this.omnom.fpsStartListening();
        }
    }
}